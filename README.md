Bill Gosper's Continued Fraction Paper in TeX
=============================================

In 1972, [Bill Gosper][] began to write a paper on algorithms for working
with continued fractions (and some related topics).  That paper was never
published (though some of its contents made their way into the continued
fractions section of [HAKMEM][]), but the unfinished draft of
[Appendix 2: Continued Fraction Arithmetic][cf] is available online.

  [Bill Gosper]: http://gosper.org
  [HAKMEM]: https://wikipedia.org/wiki/HAKMEM
  [cf]: http://www.tweedledum.com/rwg/cfup.htm

That draft is laid out the way that Bill Gosper was used to doing things
in 1972: in plain, monospaced text.  This project is an attempt to reset
the text of Bill Gosper's original draft into a more
aesthetically-pleasing LaTeX document.

A PDF generated from this code is available at https://static.aperiodic.net/gosper/cfup.pdf .


Prerequisites
-------------

You need:

 * `make`
 * XeLaTeX
 * [Rubber][]
 * Python
 * [pycairo][]
 * [NumPy][]
 * The [Latin Modern][] fonts

  [Rubber]: https://gitlab.com/latex-rubber/rubber
  [pycairo]: https://www.cairographics.org/pycairo/
  [NumPy]: http://www.numpy.org/
  [Latin Modern]: http://www.gust.org.pl/projects/e-foundry/latin-modern/


Building the Paper
------------------

    make

The result will be in `cf.pdf`.
